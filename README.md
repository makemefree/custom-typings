# Custom Typings

Add any custom typings that needs to be defined for any project add here.

For using in another lib use below command with the new commit id:
```sh
    npm link ../custom-typings
```
