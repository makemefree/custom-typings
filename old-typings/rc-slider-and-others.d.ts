import React = __React;

declare module 'react-pure-render/function' {
  class shouldPureComponentUpdate extends React.Component<{}, {}>{}
  export default shouldPureComponentUpdate
}

declare module 'google-map-react' {
  class GoogleMap extends React.Component<{ center?: Array<number>;zoom?:number}, {}>{}
  export default GoogleMap
}

declare module 'rc-slider/Handle' {
  export default class Handle extends React.Component<{},{}> {
     constructor(props: any);
     showTooltip(): void;
     hideTooltip(): void;
     render(): any;
    }
 }

declare module 'rc-slider/Marks' {
  const Marks: ({className, vertical, marks, included, upperBound, lowerBound, max, min}: {
     className: any;
     vertical: any;
     marks: any;
     included: any;
     upperBound: any;
     lowerBound: any;
     max: any;
     min: any;
 }) => void;
 export default Marks;
}

declare module 'rc-slider/Track' {const Track: ({className, included, vertical, offset, length}: {
     className: any;
     included: any;
     vertical: any;
     offset: any;
     length: any;
 }) => any;
 export default Track;

}

declare module 'rc-slider/Steps' {const Steps: ({prefixCls, vertical, marks, dots, step, included, lowerBound, upperBound, max, min}: {
     prefixCls: any;
     vertical: any;
     marks: any;
     dots: any;
     step: any;
     included: any;
     lowerBound: any;
     upperBound: any;
     max: any;
     min: any;
 }) => any;
 export default Steps;

}

declare module 'rc-slider' {
    interface rcProps {
      min?: number;
      max?: number;
      step?: number;
      defaultValue?:any;
      value?: any;
      marks?:any;
      included?:boolean;
      className?: string;
      prefixCls?: string;
      disabled?:boolean;
      children?: any;
      onBeforeChange?: any;
      onChange?: any;
      onAfterChange?: any
      tipTransitionName?: string;
      tipFormatter?: any;
      dots?:boolean;
      range?:boolean;
      vertical?:boolean;
      allowCross?:boolean;
    }

     class Slider extends React.Component<rcProps, {}> {
         constructor(props: any);
         componentWillReceiveProps(nextProps: any): void;
         onChange(state: any): void;
         onMouseMove(e: any): void;
         onTouchMove(e: any): void;
         onMove(e: any, position: any): void;
         onTouchStart(e: any): void;
         onMouseDown(e: any): void;
         onStart(position: any): void;
         getValue(): any;
         getSliderLength(): any;
         getSliderStart(): any;
         getPrecision(step: any): number;
         isValueOutOfBounds(value: any, props: any): boolean;
         trimAlignValue(v: any, nextProps: any): void;
         render(): any;
     }
     export default Slider;
 }

declare function handleQuery(tokens: any, options: any, params: any): {
    value: any;
    key: any;
    references: any;
    parents: any;
};
declare function handleToken(token: any, state: any): boolean;
declare function matches(item: any, parts: any): boolean;
declare function isDefined(value: any): boolean;
declare function shouldOverride(state: any, key: any): boolean;
declare function isDeepAccessor(currentItem: any, key: any): boolean;
declare function getLastParentObject(parents: any): any;
 
