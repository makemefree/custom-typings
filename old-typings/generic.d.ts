/// <reference path="typings/main.d.ts" />

declare module _ {
    interface LoDashStatic {
        concat(val?: any, val1?: any): any;
        upperCase(val?: any);
        uniq(val1?: any, val2?: any) : any;
   }
}

declare interface Window {
    initmaps: ()=>void;
    mapLibLoaded: number;
    perf:any;
    Perf:any;
    __data:any;
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__:any;
}

declare module "react-height" {
    import React = __React

    interface ReactHeightProps extends React.HTMLAttributes, React.Props<ReactHeight> {
        onHeightReady?: (height : number) => void;
        hidden?: boolean
        dirty?: boolean
    }

    export default class ReactHeight extends React.Component<ReactHeightProps, any> { }
}
declare var teconfig:any;
/** using node typing instead of this
declare var module:any; **/
