/// <reference path="typings/main.d.ts" />

interface JQuery {
    form(val?: any, val1?:any, val2?:any): JQuery;
    transition(val?: any): JQuery;
    dropdown(val?:any,val1?:any): any;
    popup(val: any): any;
    modal(val?: any,val1?:any,val2?:any): any;
    tab(val?:any,val2?:any): any;
    trumbowyg(val?: any):any;
    progress(val?:any): any;
    dimmer(val?: any): any;
    menu(val?:any): any;
    accordion(val?:any): any;
    vegas(val?: any): any;
    calendar(val?: any):any;
    rating(val?: any,val1?: any,val2?: any): any;
    sticky(val?: any): any;
    visibility(val?: any): any;
    slider(val?: any): any;
    message(val?: any): any;
    sidebar(val?:any, val1?:any);
    DataTable(val?:any):any;
    shape(val?: any,val1?:any):any;
    ckeditor(val?: any,val1?:any):any;
    checkbox(val?:any):any;
    smint(val1?:any):any;
    embed(val?:any,val1?:any): any;
}

declare var CKEDITOR : any;
declare var unescape:(val: any)=> any;

interface JQueryStatic {
    tab(val?:any,val2?:any): any;
}
