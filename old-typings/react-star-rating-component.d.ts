declare module 'react-star-rating-component' {

    import React = __React

	interface StarRatingProps {
        name:string;
        value:number;
        starCount:number;
        onStarClick?:(nextValue:number, prevValue:number, name:string)=>void
        renderStarIcon?:(nextValue:number, prevValue:number, name:string)=>any
        starColor?:string; /* color of selected icons, default `#ffb400` */
        editing : boolean /* is component available for editing, default `true` */
    }

	export default class StarRatingComponent extends React.Component<StarRatingProps,{}> {}
}
