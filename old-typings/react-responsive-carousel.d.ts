declare module "react-responsive-carousel" {
  import ReactRC = __React;
  interface CarouselProps {
      showArrows?:boolean;
      showStatus?:boolean;
      showIndicators?:boolean;
      showThumbs?:boolean;
      selectedItem?:number;
      axis?:string;
      onChange?:()=>void;
      onClickItem?:()=>void;
      onClickThumb?:()=>void;
      key?:string;
  }
  class Carousel extends ReactRC.Component<CarouselProps,{}>{

  }
}