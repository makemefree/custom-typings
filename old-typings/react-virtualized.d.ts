/// <reference path='../react/index.d.ts' />

declare module "react-virtualized" {
  import ReactV = __React;
  interface VirtualScrollProps {
      className?:string;
      height:number;
      onRowsRendered?:(overscanStartIndex: number, overscanStopIndex: number, startIndex: number, stopIndex: number)=>void;
      onScroll?:( clientHeight: number, scrollHeight: number, scrollTop: number )=> void;
      noRowsRenderer?:()=>any;
      rowHeight:number;
      rowRenderer:(index: number, isScrolling: boolean)=>any;
      overscanRowCount?:number;
      rowCount:number;
      scrollToAlignment?:string;
      scrollToIndex?:number;
      scrollTop?:number;
      style?:Object;
      width:number;
      autoHeight?:boolean;
  }
  class VirtualScroll extends ReactV.Component<VirtualScrollProps,{}>{

  }

  interface WindowScrollerProps {
    children?:any;
    onResize?:any
    onScroll?:any;
  }
  class WindowScroller extends ReactV.Component<WindowScrollerProps,{}>{

  }
  class AutoSizer extends ReactV.Component<{children?:any;
    disableHeight?:boolean;disableWidth?:boolean;onResize?:any},{}>{
  }
}
