declare module "react-collapse" {
    import React = __React

   //Taken from react-motion/index.d.ts file
   interface SpringHelperConfig {
        stiffness: number;
        damping: number;
        precision?: number;
    }

    interface CollapseProps extends React.HTMLAttributes, React.Props<Collapse> {
        isOpened?: boolean
        fixedHeight?: number
        springConfig?: SpringHelperConfig
        keepCollapsedContent?: boolean
        onHeightReady?: (height : number) => void
    }

    export default class Collapse extends React.Component<CollapseProps, any> { }
}
