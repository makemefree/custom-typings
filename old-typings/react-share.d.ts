declare module "react-share" {
    import React = __React
    class FacebookShareButton extends React.Component<any, any> { }
    class GooglePlusShareButton extends React.Component<any, any> { }
    class LinkedinShareButton extends React.Component<any, any> { }
    class TwitterShareButton extends React.Component<any, any> { }
    class PinterestShareButton extends React.Component<any, any> { }
    class VKShareButton extends React.Component<any, any> { }
    class TelegramShareButton extends React.Component<any, any> { }
    class WhatsappShareButton extends React.Component<any, any> { }
    export var ShareButtons: {
        FacebookShareButton,
        GooglePlusShareButton,
        LinkedinShareButton,
        TwitterShareButton,
        PinterestShareButton,
        VKShareButton,
        TelegramShareButton,
        WhatsappShareButton
    };
    export function generateShareIcon(a: string): any;
}
