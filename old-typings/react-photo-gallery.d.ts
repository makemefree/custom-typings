declare module 'react-photo-gallery' {
	import React = __React

	interface LightboxImage{
		src?:string;
		srcset?:Array<string>;
		caption?:string;
	}
	interface PhotoSet{
    src: string;
    width: number;
    height: number,
    aspectRatio: number,
    lightboxImage?:LightboxImage
	}
	interface GalleryProps {
		photos:PhotoSet;
		disableLightbox:boolean;
		lightboxShowImageCount?: boolean;
		backdropClosesModal?:boolean;
	}
	class Gallery extends React.Component<GalleryProps,{}> {

	}
	export default Gallery;

}
