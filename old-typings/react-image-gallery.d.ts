declare module 'react-image-gallery' {
	import React = __React;
	interface ReactImageGalleryProps{
		items?: Array<any>;
		showNav?: boolean;
		autoPlay?: boolean;
		lazyLoad?: boolean;
		infinite?: boolean;
		showIndex?: boolean;
		showBullets?: boolean;
		showThumbnails?: boolean;
		slideOnThumbnailHover?: boolean;
		disableThumbnailScroll?: boolean;
		defaultImage?: string;
		indexSeparator?: string;
		startIndex?: number;
		slideInterval?: number;
		onSlide?: (slideIndex: number) => void;
		onPause?: (slideIndex: number) => void;
		onPlay?: (slideIndex: number) => void;
		onClick?: (event?:any) => void;
		onImageLoad?: (event?:any) => void;
	}
	export default class ImageGallery extends React.Component<ReactImageGalleryProps, {}> {
	    constructor(props: any);
	    componentDidUpdate(prevProps: any, prevState: any): void;
	    componentWillMount(): void;
	    componentDidMount(): void;
	    componentWillUnmount(): void;
	    play(callback?: boolean): void;
	    pause(callback?: boolean): void;
	    slideToIndex(index: any, event: any): void;
	    _wrapClick(func: any): (event: any) => void;
	    _touchEnd(): void;
	    _handleResize(): void;
	    _handleKeyDown(event: any): void;
	    _handleMouseOverThumbnails(index: any): void;
	    _handleMouseLeaveThumbnails(): void;
	    _handleMouseOver(): void;
	    _handleMouseLeave(): void;
	    _handleImageError(event: any): void;
	    _handleOnSwiped(ev: any, x: any, y: any, isFlick: any): void;
	    _handleOnSwipedTo(index: any): void;
	    _handleSwiping(index: any, _: any, delta: any): void;
	    _canNavigate(): boolean;
	    _canSlideLeft(): boolean;
	    _canSlideRight(): boolean;
	    _updateThumbnailTranslateX(prevState: any): void;
	    _setThumbsTranslateX(thumbsTranslateX: any): void;
	    _getThumbsTranslateX(indexDifference: any): number;
	    _getAlignmentClassName(index: any): string;
	    _getSlideStyle(index: any): {
	        WebkitTransform: string;
	        MozTransform: string;
	        msTransform: string;
	        OTransform: string;
	        transform: string;
	        zIndex: number;
	    };
	    _getThumbnailStyle(): {
	        WebkitTransform: string;
	        MozTransform: string;
	        msTransform: string;
	        OTransform: string;
	        transform: string;
	    };
	    _slideLeft(): void;
	    _slideRight(): void;
	    render(): any;
	}

}
