declare module 'react-image-lightbox-universal' {
	import React = __React

	interface LightboxProps {
		mainSrc:string;
        prevSrc?:string;
        nextSrc?:string;
		mainSrcThumbnail?:string;
        prevSrcThumbnail?:string;
        nextSrcThumbnail?:string;
        onCloseRequest?:()=>void;
        onMovePrevRequest?:()=>void;
        onMoveNextRequest?:()=>void;
        discourageDownloads?:boolean;
        animationDisabled?:boolean;
        animationOnKeyInput?:boolean;
        animationDuration?:number;
        keyRepeatLimit?:number;
        keyRepeatKeyupBonus?:number;
        imageTitle?:string;
        toolbarButtons?:Array<any>;
        imagePadding?:number;
        clickOutsideToClose?:boolean;
	}

	export default class Lightbox extends React.Component<LightboxProps,{}> {}
}
