declare interface Window {
  initmaps: () => void;
  mapLibLoaded: number;
  perf: any;
  Perf: any;
  navigator: any;
  gapi: any;
  FB: any;
  fbAsyncInit: any;
  __data: any;
  __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any;
}

declare var teconfig: any;

declare var require: {
  (path: string): any;
  <T>(path: string): T;
  (paths: string[], callback: (...modules: any[]) => void): void;
  ensure: (
    paths: string[],
    callback: (require: <T>(path: string) => T) => void
  ) => void;
};

declare var module: any;
declare var process: any;

//Cloudinary
declare var cloudinary: any;
